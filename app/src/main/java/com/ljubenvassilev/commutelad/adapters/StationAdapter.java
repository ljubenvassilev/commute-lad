package com.ljubenvassilev.commutelad.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ljubenvassilev.commutelad.model.Station;

import java.util.List;

public class StationAdapter extends ArrayAdapter<Station> {

    private List<Station> stations;

    public StationAdapter(@NonNull Context context, int resource, List<Station> stations) {
        super(context, resource);
        this.stations = stations;
    }

    @Override
    public int getCount() {
        return stations.size();
    }

    @Nullable
    @Override
    public Station getItem(int position) {
        return stations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(stations.get(position).getStationDesc());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(stations.get(position).getStationDesc());
        return label;
    }

    public void setStationList(List<Station> stations) {
        this.stations = stations;
        notifyDataSetChanged();
    }
}
