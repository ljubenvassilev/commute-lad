package com.ljubenvassilev.commutelad.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ljubenvassilev.commutelad.viewholders.CommuteViewHolder;
import com.ljubenvassilev.commutelad.R;
import com.ljubenvassilev.commutelad.model.Commute;

import java.util.List;

public class CommuteAdapter extends RecyclerView.Adapter<CommuteViewHolder> {

    private List<Commute> commuteList;
    private IChangeEnabled listener;

    public CommuteAdapter(List<Commute> commuteList, IChangeEnabled listener) {
        this.commuteList = commuteList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CommuteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CommuteViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.commute_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CommuteViewHolder holder, int position) {
        Commute commute = commuteList.get(position);
        holder.title.setText(commute.getTitle());
        holder.enabled.setChecked(commute.isEnabled());
        holder.stations.setText(commute.getStartStation().concat(" -> ")
                .concat(commute.getDestinationStation()));
        int time = commute.getTime();
        int hours = time / 60;
        int minutes = time % 60;
        holder.time.setText(String.valueOf(hours).concat(":")
                .concat(minutes < 10 ? "0".concat(String.valueOf(minutes)):String.valueOf(minutes)));
        String daysString = "";
        if (commute.isMonday()) daysString += "Mon, ";
        if (commute.isTuesday()) daysString += "Tue, ";
        if (commute.isWednesday()) daysString += "Wed, ";
        if (commute.isThursday()) daysString += "Thu, ";
        if (commute.isFriday()) daysString += "Fri, ";
        if (commute.isSaturday()) daysString += "Sat, ";
        if (commute.isSunday()) daysString += "Sun, ";
        if (daysString.length() > 0) {
            daysString = daysString.substring(0, daysString.length() - 2);
        }
        holder.days.setText(daysString);
        holder.enabled.setOnCheckedChangeListener((buttonView, isChecked) ->
                listener.onChange(commuteList.get(position), isChecked));
    }

    @Override
    public int getItemCount() {
        return commuteList.size();
    }

    public void setCommuteList(List<Commute> commuteList) {
        this.commuteList = commuteList;
        notifyDataSetChanged();
    }

    public interface IChangeEnabled {
        void onChange(Commute commute, boolean enabled);
    }
}
