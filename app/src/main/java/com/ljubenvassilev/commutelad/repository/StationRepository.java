package com.ljubenvassilev.commutelad.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.lifecycle.LiveData;

import com.ljubenvassilev.commutelad.api.ApiProvider;
import com.ljubenvassilev.commutelad.db.CommuteRoomDatabase;
import com.ljubenvassilev.commutelad.db.StationDao;
import com.ljubenvassilev.commutelad.model.AllStations;
import com.ljubenvassilev.commutelad.model.Station;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StationRepository {

    private StationDao stationDao;
    private LiveData<List<Station>> allStations;

    public StationRepository(Application application) {
        CommuteRoomDatabase db = CommuteRoomDatabase.getDatabase(application);
        stationDao = db.getStationDao();
        allStations = stationDao.getAllStations();

        Call<AllStations> stationsList = ApiProvider.getInstance().getApiService().getAllStations();
        stationsList.enqueue(new Callback<AllStations>() {
            @Override
            public void onResponse(@NotNull Call<AllStations> call,
                                   @NotNull Response<AllStations> response) {
                Completable.fromAction(() -> {
                    if (response.body() != null) {
                        new InsertAsyncTask(stationDao).execute(response.body().stationList);
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new CompletableObserver() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {

                            }

                            @Override
                            public void onComplete() {
                                Toast.makeText(application,"Stations updated!",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                Toast.makeText(application,"Stations update failed!",Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            @Override
            public void onFailure(@NotNull Call<AllStations> call, @NotNull Throwable t) {
                Toast.makeText(application,"Stations update failed!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public LiveData<List<Station>> getAllStations() {
        return allStations;
    }

    private static class InsertAsyncTask extends AsyncTask<List<Station>, Void, Void> {

        private StationDao mAsyncTaskDao;

        InsertAsyncTask(StationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<Station>... params) {
            for (Station station : params[0]) {
                mAsyncTaskDao.insert(station);
            }
            return null;
        }
    }
}
