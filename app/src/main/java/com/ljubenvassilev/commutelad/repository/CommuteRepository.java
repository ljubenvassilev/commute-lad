package com.ljubenvassilev.commutelad.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.db.CommuteDao;
import com.ljubenvassilev.commutelad.db.CommuteRoomDatabase;

import java.util.List;

enum Command {
    CREATE,
    UPDATE,
    DELETE
}

public class CommuteRepository {

    private CommuteDao commuteDao;
    private LiveData<List<Commute>> allCommutes;

    public CommuteRepository(Context context) {
        CommuteRoomDatabase db = CommuteRoomDatabase.getDatabase(context);
        commuteDao = db.getCommuteDao();
        allCommutes = commuteDao.getAllCommutes();
    }

    public LiveData<Commute> getCommuteById(int id) {
        return commuteDao.getCommuteById(id);
    }

    public void getCommuteByJob(int job, GetCommuteByJobAsyncTask.Callback callback) {
        new GetCommuteByJobAsyncTask(commuteDao, callback).execute(job);
    }

    public static class GetCommuteByJobAsyncTask extends AsyncTask<Integer, Void, Commute> {

        private CommuteDao mAsyncTaskDao;
        private Callback callback;

        public interface Callback {
            void onComplete(Commute commute);
        }

        GetCommuteByJobAsyncTask(CommuteDao dao, Callback callback) {
            mAsyncTaskDao = dao;
            this.callback = callback;
        }

        @Override
        protected Commute doInBackground(final Integer... params) {
            return mAsyncTaskDao.getCommuteByJob(params[0]);
        }

        @Override
        protected void onPostExecute(Commute commute) {
            super.onPostExecute(commute);
            callback.onComplete(commute);
        }
    }


    public LiveData<List<Commute>> getAllCommutes() {
        return allCommutes;
    }

    public void insert(Commute commute) {
        new CRUDAsyncTask(commuteDao, Command.CREATE).execute(commute);
    }

    public void updateCommute(Commute commute) {
        new CRUDAsyncTask(commuteDao, Command.UPDATE).execute(commute);
    }

    public void deleteCommute(Commute commute) {
        new CRUDAsyncTask(commuteDao, Command.DELETE).execute(commute);
    }

    private static class CRUDAsyncTask extends AsyncTask<Commute, Void, Void> {

        private CommuteDao mAsyncTaskDao;
        private Command command;

        CRUDAsyncTask(CommuteDao dao, Command command) {
            mAsyncTaskDao = dao;
            this.command = command;
        }

        @Override
        protected Void doInBackground(final Commute... params) {
            switch (command) {
                case CREATE:
                    mAsyncTaskDao.insert(params[0]);
                    break;
                case UPDATE:
                    mAsyncTaskDao.update(params[0]);
                    break;
                case DELETE:
                    mAsyncTaskDao.delete(params[0]);
            }
            return null;
        }
    }
}
