package com.ljubenvassilev.commutelad.util;

public interface SwipeControllerActions {

    void onLeftClicked(int position);

    void onRightClicked(int position);

}
