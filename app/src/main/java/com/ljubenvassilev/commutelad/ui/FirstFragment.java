package com.ljubenvassilev.commutelad.ui;

import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ljubenvassilev.commutelad.adapters.CommuteAdapter;
import com.ljubenvassilev.commutelad.R;
import com.ljubenvassilev.commutelad.util.SwipeController;
import com.ljubenvassilev.commutelad.util.SwipeControllerActions;
import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.viewmodel.FirstFragmentViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class FirstFragment extends Fragment {

    private RecyclerView recyclerView;
    private FirstFragmentViewModel commuteModel;
    private List<Commute> commuteList;
    private CommuteAdapter commuteAdapter;
    private SwipeController swipeController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.floatingActionButton)
                .setOnClickListener(view1 -> NavHostFragment
                        .findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment));

        commuteModel = new ViewModelProvider(this).get(FirstFragmentViewModel.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));

        commuteList = new ArrayList<>();
        commuteAdapter = new CommuteAdapter(commuteList, (commute, enabled) ->
                commuteModel.changeCommute(getContext(), commute, enabled));
        recyclerView.setAdapter(commuteAdapter);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(@NotNull Canvas c, @NotNull RecyclerView parent,
                               @NotNull RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onLeftClicked(int position) {
                FirstFragmentDirections.ActionFirstFragmentToSecondFragment action =
                        FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                action.setCommuteId(commuteList.get(position).getId());
                NavHostFragment.findNavController(FirstFragment.this).navigate(action);
            }

            @Override
            public void onRightClicked(int position) {
                commuteModel.deleteCommute(getContext(), commuteList.get(position));
            }
        });
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);
        commuteModel.getListLiveData().observe(getViewLifecycleOwner(), commutes -> {
            commuteAdapter.setCommuteList(commutes);
            commuteList = commutes;
        });
    }

}
