package com.ljubenvassilev.commutelad.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.ljubenvassilev.commutelad.R;
import com.ljubenvassilev.commutelad.adapters.StationAdapter;
import com.ljubenvassilev.commutelad.exceptions.EmptyTitleException;
import com.ljubenvassilev.commutelad.exceptions.NoDaySelectedException;
import com.ljubenvassilev.commutelad.exceptions.SameStationsException;
import com.ljubenvassilev.commutelad.model.Station;
import com.ljubenvassilev.commutelad.viewmodel.SecondFragmentViewModel;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;
import java.util.List;

public class SecondFragment extends Fragment {

    private SecondFragmentViewModel secondFragmentViewModel;
    private List<Station> stationList;
    private Spinner startSpinner;
    private StationAdapter startStationAdapter;
    private Spinner destinationSpinner;
    private StationAdapter destinationStationAdapter;
    private NumberPicker numberPicker;
    private TextInputEditText title;
    private MaterialCheckBox monday, tuesday, wednesday, thursday, friday, saturday, sunday;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title = view.findViewById(R.id.title);
        monday = view.findViewById(R.id.monday);
        tuesday = view.findViewById(R.id.tuesday);
        wednesday = view.findViewById(R.id.wednesday);
        thursday = view.findViewById(R.id.thursday);
        friday = view.findViewById(R.id.friday);
        saturday = view.findViewById(R.id.saturday);
        sunday = view.findViewById(R.id.sunday);

        secondFragmentViewModel = new ViewModelProvider(this)
                .get(SecondFragmentViewModel.class);
        int commuteId = SecondFragmentArgs.fromBundle(getArguments()).getCommuteId();
        if (commuteId >= 0) {
            secondFragmentViewModel.getCommuteToEdit(commuteId).observe(getViewLifecycleOwner(),
                    commute -> {
                if (commute != null) {
                    secondFragmentViewModel.setId(commuteId);
                    title.setText(commute.getTitle());
                    secondFragmentViewModel.setDistance(commute.getDistance());
                    numberPicker.setValue(commute.getDistance());
                    int time = commute.getTime();
                    secondFragmentViewModel.setTime(time);
                    int hourOfDay = time / 60;
                    int minute = time % 60;
                    String timeText = String.valueOf(hourOfDay).concat(":").concat(minute < 10 ? "0"
                            .concat(String.valueOf(minute)) : String.valueOf(minute));
                    ((TextView)view.findViewById(R.id.timeText)).setText(timeText);
                    monday.setChecked(commute.isMonday());
                    tuesday.setChecked(commute.isTuesday());
                    wednesday.setChecked(commute.isWednesday());
                    thursday.setChecked(commute.isThursday());
                    friday.setChecked(commute.isFriday());
                    saturday.setChecked(commute.isSaturday());
                    sunday.setChecked(commute.isSunday());
                    if (stationList != null && stationList.size() > 0) {
                        for (Station station : stationList) {
                            if (station.getStationDesc().equalsIgnoreCase(commute
                                    .getDestinationStation())) {
                                secondFragmentViewModel.setDestination(station);
                            } else if (station.getStationDesc().equalsIgnoreCase(commute
                                    .getStartStation())) {
                                secondFragmentViewModel.setStart(station);
                            }
                        }
                    }
                }
            });
        }

        view.findViewById(R.id.timeButton).setOnClickListener(v -> {
            DialogFragment newFragment = new TimePickerFragment((view12, hourOfDay, minute) -> {
                String time = String.valueOf(hourOfDay).concat(":").concat(minute < 10 ? "0"
                        .concat(String.valueOf(minute)) : String.valueOf(minute));
                ((TextView)view.findViewById(R.id.timeText)).setText(time);
                secondFragmentViewModel.setTime((60 * hourOfDay) + minute);
            }, secondFragmentViewModel.getTime());
            newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
        });

        stationList = new ArrayList<>();
        startSpinner = view.findViewById(R.id.startingStation);
        destinationSpinner = view.findViewById(R.id.destinationStation);
        startStationAdapter = new StationAdapter(getContext(), android.R.layout.simple_spinner_item,
                stationList);
        destinationStationAdapter = new StationAdapter(getContext(),
                android.R.layout.simple_spinner_item, stationList);
        startSpinner.setAdapter(startStationAdapter);
        destinationSpinner.setAdapter(destinationStationAdapter);
        startSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                secondFragmentViewModel.setStart(stationList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        destinationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                secondFragmentViewModel.setDestination(stationList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        secondFragmentViewModel.getListLiveData().observe(getViewLifecycleOwner(), stations -> {
            stationList = stations;
            startStationAdapter.setStationList(stations);
            destinationStationAdapter.setStationList(stations);
            if (secondFragmentViewModel.getId() >= 0) {
                secondFragmentViewModel.getCommuteToEdit(commuteId).observe(getViewLifecycleOwner(),
                        commute -> {
                            for (int i = 0; i < stationList.size(); i++) {
                                String search = stationList.get(i).getStationDesc();
                                if (search.equalsIgnoreCase(commute.getStartStation())) {
                                    startSpinner.setSelection(i);
                                } else if (search.equalsIgnoreCase(commute.getDestinationStation())) {
                                    destinationSpinner.setSelection(i);
                                }
                            }
                        });
            }
        });

        numberPicker = view.findViewById(R.id.distance);
        secondFragmentViewModel.setDistance(0);
        numberPicker.setOnValueChangedListener((picker, oldVal, newVal) ->
                secondFragmentViewModel.setDistance(newVal));

        view.findViewById(R.id.saveButton).setOnClickListener(view1 -> {
            try {
                secondFragmentViewModel.createCommute(title.getText().toString(), monday.isChecked(),
                        tuesday.isChecked(), wednesday.isChecked(), thursday.isChecked(),
                        friday.isChecked(), saturday.isChecked(), sunday.isChecked(), commuteId);
            } catch (EmptyTitleException e) {
                title.setError("Title cannot be empty!");
                return;
            } catch (NoDaySelectedException e) {
                Toast.makeText(getContext(),"No day selected!", Toast.LENGTH_SHORT).show();
                return;
            } catch (SameStationsException e) {
                Toast.makeText(getContext(),"Start and destination cannot be the same!",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            ((InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
            NavHostFragment.findNavController(SecondFragment.this)
                    .navigate(R.id.action_SecondFragment_to_FirstFragment);
        });
    }
}
