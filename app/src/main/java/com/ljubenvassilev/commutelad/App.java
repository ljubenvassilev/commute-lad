package com.ljubenvassilev.commutelad;

import android.app.Application;

import com.evernote.android.job.JobManager;
import com.ljubenvassilev.commutelad.jobs.CommuteJobCreator;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JobManager.create(this).addJobCreator(new CommuteJobCreator());
    }
}