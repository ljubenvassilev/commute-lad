package com.ljubenvassilev.commutelad.viewholders;

import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ljubenvassilev.commutelad.R;

public class CommuteViewHolder extends RecyclerView.ViewHolder {

    public TextView title, stations, time, days;
    public Switch enabled;

    public CommuteViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
        stations = itemView.findViewById(R.id.stations);
        time = itemView.findViewById(R.id.time);
        days = itemView.findViewById(R.id.days);
        enabled = itemView.findViewById(R.id.enabledSwitch);
    }
}
