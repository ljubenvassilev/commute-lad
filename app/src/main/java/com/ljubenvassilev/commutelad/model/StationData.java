package com.ljubenvassilev.commutelad.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="objStationData", strict = false)
public class StationData {

    @Element(name = "Servertime", required = false)
    private String serverTime;

    @Element(name = "Traincode")
    private String trainCode;

    @Element(name = "Stationfullname", required = false)
    private String stationFullName;

    @Element(name = "Stationcode", required = false)
    private String stationCode;

    @Element(name = "Querytime", required = false)
    private String queryTime;

    @Element(name = "Traindate", required = false)
    private String trainDate;

    @Element(name = "Origin", required = false)
    private String origin;

    @Element(name = "Destination", required = false)
    private String destination;

    @Element(name = "Origintime", required = false)
    private String originTime;

    @Element(name = "Destinationtime", required = false)
    private String destinationTime;

    @Element(name = "Status", required = false)
    private String status;

    @Element(name = "Lastlocation", required = false)
    private String lastLocation;

    @Element(name = "Duein", required = false)
    private String dueIn;

    @Element(name = "Late", required = false)
    private String late;

    @Element(name = "Exparrival", required = false)
    private String expArrival;

    @Element(name = "Expdepart", required = false)
    private String expDepart;

    @Element(name = "Scharrival", required = false)
    private String schArrival;

    @Element(name = "Schdepart", required = false)
    private String schDepart;

    @Element(name = "Direction", required = false)
    private String direction;

    @Element(name = "Traintype", required = false)
    private String trainType;

    @Element(name = "Locationtype", required = false)
    private String locationType;

    public StationData() {}

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getTrainCode() {
        return trainCode;
    }

    public void setTrainCode(String trainCode) {
        this.trainCode = trainCode;
    }

    public String getStationFullName() {
        return stationFullName;
    }

    public void setStationFullName(String stationFullName) {
        this.stationFullName = stationFullName;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(String queryTime) {
        this.queryTime = queryTime;
    }

    public String getTrainDate() {
        return trainDate;
    }

    public void setTrainDate(String trainDate) {
        this.trainDate = trainDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginTime() {
        return originTime;
    }

    public void setOriginTime(String originTime) {
        this.originTime = originTime;
    }

    public String getDestinationTime() {
        return destinationTime;
    }

    public void setDestinationTime(String destinationTime) {
        this.destinationTime = destinationTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(String lastLocation) {
        this.lastLocation = lastLocation;
    }

    public String getDueIn() {
        return dueIn;
    }

    public void setDueIn(String dueIn) {
        this.dueIn = dueIn;
    }

    public String getLate() {
        return late;
    }

    public void setLate(String late) {
        this.late = late;
    }

    public String getExpArrival() {
        return expArrival;
    }

    public void setExpArrival(String expArrival) {
        this.expArrival = expArrival;
    }

    public String getExpDepart() {
        return expDepart;
    }

    public void setExpDepart(String expDepart) {
        this.expDepart = expDepart;
    }

    public String getSchArrival() {
        return schArrival;
    }

    public void setSchArrival(String schArrival) {
        this.schArrival = schArrival;
    }

    public String getSchDepart() {
        return schDepart;
    }

    public void setSchDepart(String schDepart) {
        this.schDepart = schDepart;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
