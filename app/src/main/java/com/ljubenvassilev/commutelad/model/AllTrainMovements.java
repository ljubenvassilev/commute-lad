package com.ljubenvassilev.commutelad.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ArrayOfObjTrainMovements")
public class AllTrainMovements {
    @ElementList(inline = true, required = false)
    public List<TrainMovement> trainMovementList;
}
