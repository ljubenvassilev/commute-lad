package com.ljubenvassilev.commutelad.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ArrayOfObjStationData")
public class AllStationData {
    @ElementList(inline = true, required = false)
    public List<StationData> stationDataList;
}
