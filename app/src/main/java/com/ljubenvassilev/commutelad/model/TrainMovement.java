package com.ljubenvassilev.commutelad.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="objTrainMovements", strict = false)
public class TrainMovement {

    @Element(name = "TrainCode", required = false)
    private String trainCode;

    @Element(name = "TrainDate", required = false)
    private String trainDate;

    @Element(name = "LocationCode", required = false)
    private String locationCode;

    @Element(name = "LocationFullName")
    private String locationFullName;

    @Element(name = "LocationOrder", required = false)
    private String locationOrder;

    @Element(name = "LocationType")
    private String locationType;

    @Element(name = "TrainOrigin", required = false)
    private String trainOrigin;

    @Element(name = "TrainDestination", required = false)
    private String trainDestination;

    @Element(name = "ScheduledArrival", required = false)
    private String scheduledArrival;

    @Element(name = "ScheduledDeparture", required = false)
    private String scheduledDeparture;

    @Element(name = "ExpectedArrival", required = false)
    private String expectedArrival;

    @Element(name = "ExpectedDeparture", required = false)
    private String expectedDeparture;

    @Element(name = "Arrival", required = false)
    private String arrival;

    @Element(name = "Departure", required = false)
    private String departure;

    @Element(name = "AutoArrival", required = false)
    private String autoArrival;

    @Element(name = "AutoDepart", required = false)
    private String autoDepart;

    @Element(name = "StopType", required = false)
    private String stopType;

    public TrainMovement() {}

    public String getTrainCode() {
        return trainCode;
    }

    public void setTrainCode(String trainCode) {
        this.trainCode = trainCode;
    }

    public String getTrainDate() {
        return trainDate;
    }

    public void setTrainDate(String trainDate) {
        this.trainDate = trainDate;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationFullName() {
        return locationFullName;
    }

    public void setLocationFullName(String locationFullName) {
        this.locationFullName = locationFullName;
    }

    public String getLocationOrder() {
        return locationOrder;
    }

    public void setLocationOrder(String locationOrder) {
        this.locationOrder = locationOrder;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getTrainOrigin() {
        return trainOrigin;
    }

    public void setTrainOrigin(String trainOrigin) {
        this.trainOrigin = trainOrigin;
    }

    public String getTrainDestination() {
        return trainDestination;
    }

    public void setTrainDestination(String trainDestination) {
        this.trainDestination = trainDestination;
    }

    public String getScheduledArrival() {
        return scheduledArrival;
    }

    public void setScheduledArrival(String scheduledArrival) {
        this.scheduledArrival = scheduledArrival;
    }

    public String getScheduledDeparture() {
        return scheduledDeparture;
    }

    public void setScheduledDeparture(String scheduledDeparture) {
        this.scheduledDeparture = scheduledDeparture;
    }

    public String getExpectedArrival() {
        return expectedArrival;
    }

    public void setExpectedArrival(String expectedArrival) {
        this.expectedArrival = expectedArrival;
    }

    public String getExpectedDeparture() {
        return expectedDeparture;
    }

    public void setExpectedDeparture(String expectedDeparture) {
        this.expectedDeparture = expectedDeparture;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getAutoArrival() {
        return autoArrival;
    }

    public void setAutoArrival(String autoArrival) {
        this.autoArrival = autoArrival;
    }

    public String getAutoDepart() {
        return autoDepart;
    }

    public void setAutoDepart(String autoDepart) {
        this.autoDepart = autoDepart;
    }

    public String getStopType() {
        return stopType;
    }

    public void setStopType(String stopType) {
        this.stopType = stopType;
    }
}
