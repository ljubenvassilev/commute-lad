package com.ljubenvassilev.commutelad.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ArrayOfObjStation")
public class AllStations {
    @ElementList(inline = true)
    public List<Station> stationList;
}
