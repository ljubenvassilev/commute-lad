package com.ljubenvassilev.commutelad.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="objStation", strict = false)
@Entity(tableName = "station_table")
public class Station {

    @Element(name = "StationDesc")
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "desc")
    private String stationDesc;

    @Element(name = "StationAlias", required = false)
    @ColumnInfo(name = "alias")
    private String stationAlias;

    @Element(name = "StationLatitude", required = false)
    @ColumnInfo(name = "latitude")
    private double stationLatitude;

    @Element(name = "StationLongitude", required = false)
    @ColumnInfo(name = "longitude")
    private double stationLongitude;

    @Element(name = "StationCode", required = false)
    @ColumnInfo(name = "code")
    private String stationCode;

    @Element(name = "StationId", required = false)
    @ColumnInfo(name = "id")
    private long stationId;

    public Station(){}

    @NonNull
    public String getStationDesc(){
        return this.stationDesc;
    }

    public void setStationDesc(@NonNull String stationDesc) {
        this.stationDesc = stationDesc;
    }

    @NonNull
    public String getStationAlias() {
        return stationAlias;
    }

    public void setStationAlias(String stationAlias) {
        this.stationAlias = stationAlias;
    }

    public double getStationLatitude() {
        return stationLatitude;
    }

    public void setStationLatitude(double stationLatitude) {
        this.stationLatitude = stationLatitude;
    }

    public double getStationLongitude() {
        return stationLongitude;
    }

    public void setStationLongitude(double stationLongitude) {
        this.stationLongitude = stationLongitude;
    }

    @NonNull
    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public long getStationId() {
        return stationId;
    }

    public void setStationId(long stationId) {
        this.stationId = stationId;
    }
}
