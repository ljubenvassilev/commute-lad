package com.ljubenvassilev.commutelad.jobs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.ljubenvassilev.commutelad.jobs.CommuteCheckJob;

public class CommuteJobCreator implements JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case CommuteCheckJob.TAG:
                return new CommuteCheckJob();
            default:
                return null;
        }
    }
}
