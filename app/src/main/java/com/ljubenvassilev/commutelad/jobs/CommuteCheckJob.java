package com.ljubenvassilev.commutelad.jobs;

import android.annotation.SuppressLint;
import android.icu.util.Calendar;

import androidx.annotation.NonNull;

import com.evernote.android.job.DailyJob;
import com.evernote.android.job.JobRequest;
import com.ljubenvassilev.commutelad.api.ApiProvider;
import com.ljubenvassilev.commutelad.api.ApiService;
import com.ljubenvassilev.commutelad.util.NotificationHelper;
import com.ljubenvassilev.commutelad.model.AllStationData;
import com.ljubenvassilev.commutelad.model.AllTrainMovements;
import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.model.StationData;
import com.ljubenvassilev.commutelad.model.TrainMovement;
import com.ljubenvassilev.commutelad.repository.CommuteRepository;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommuteCheckJob extends DailyJob {

    static final String TAG = "commute_job_tag";
    private CountDownLatch countDownLatch;
    private String title;
    private StringBuilder message;
    private int distance;

    @NonNull
    @Override
    protected DailyJobResult onRunDailyJob(@NonNull Params params) {
        countDownLatch = new CountDownLatch(1);
        new Thread(() -> {
            int jobId = getParams().getId();
            if (jobId >= 0) {
                CommuteRepository repository = new CommuteRepository(getContext());
                repository.getCommuteByJob(jobId, commute -> {
                    if (commute!= null && hasToRun(commute)) {
                        title = commute.getStartStation().concat(" -> ")
                                .concat(commute.getDestinationStation());
                        message = new StringBuilder();
                        distance = commute.getDistance();
                        String start = commute.getStartStation();
                        String end = commute.getDestinationStation();
                        ApiService apiService = ApiProvider.getInstance().getApiService();
                        Call<AllStationData> stationDataList = apiService
                                .getAllStationData(start);
                        stationDataList.enqueue(new Callback<AllStationData>() {
                            @Override
                            public void onResponse(@NotNull Call<AllStationData> call,
                                                   @NotNull Response<AllStationData> response) {
                                if (response.body() != null && response.body().stationDataList != null) {
                                    new Thread(() -> {
                                        CountDownLatch getAllData = new CountDownLatch(response.body().stationDataList.size());
                                        List<StationData> trainsToShow = new ArrayList<>();
                                        for (StationData stationData : response.body().stationDataList) {
                                            Call<AllTrainMovements> trainMovementsList = apiService
                                                    .getAllTrainMovements(stationData.getTrainCode(),
                                                            stationData.getTrainDate());
                                            trainMovementsList.enqueue(new Callback<AllTrainMovements>() {
                                                @Override
                                                public void onResponse(@NotNull Call<AllTrainMovements> call,
                                                                       @NotNull Response<AllTrainMovements> response) {
                                                    if (response.body() != null && response.body().trainMovementList != null) {
                                                        boolean afterStart = false;
                                                        for (TrainMovement trainMovement : response.body().trainMovementList) {
                                                            String locationFullName = trainMovement.getLocationFullName();
                                                            if (locationFullName.equalsIgnoreCase(start)) {
                                                                afterStart = true;
                                                            } else if (locationFullName.equalsIgnoreCase(end)) {
                                                                if (afterStart) {
                                                                    trainsToShow.add(stationData);
                                                                }
                                                                break;
                                                            }
                                                        }
                                                        getAllData.countDown();
                                                    } else {
                                                        getAllData.countDown();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(@NotNull Call<AllTrainMovements> call,
                                                                      @NotNull Throwable t) {
                                                    getAllData.countDown();
                                                }
                                            });
                                        }
                                        try {
                                            getAllData.await();
                                        } catch (InterruptedException ignored) {
                                        }
                                        showNotification(trainsToShow);
                                        countDownLatch.countDown();
                                    }).start();
                                }else {
                                    showNotification(new ArrayList<>());
                                }
                            }

                            @Override
                            public void onFailure(@NotNull Call<AllStationData> call,
                                                  @NotNull Throwable t) {
                                countDownLatch.countDown();
                            }
                        });
                    } else {
                        countDownLatch.countDown();
                    }
                });
            } else {
                countDownLatch.countDown();
            }
        }).start();
        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
        return DailyJobResult.SUCCESS;
    }

    private void showNotification(List<StationData> trainsToShow) {
        if (trainsToShow.size() < 1) {
            message.append("No trains found for your commute!");
        } else {
            Collections.sort(trainsToShow, (stationData1, stationData2) ->
                    stationData1.getExpDepart().compareTo(stationData2.getExpDepart()));
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat sdf = new SimpleDateFormat ("HH:mm");
            Date expectedDate = new Date();
            Date recommendedDate;
            for (StationData train : trainsToShow) {
                try {
                    Date received = sdf.parse(train.getExpDepart());
                    expectedDate.setHours(received.getHours());
                    expectedDate.setMinutes(received.getMinutes());
                } catch (ParseException ignored) {
                }
                long recommendedTime = expectedDate.getTime() - TimeUnit.MINUTES.toMillis(distance);
                recommendedDate = new Date(recommendedTime);
                String toShow = sdf.format(recommendedDate);
                message.append(train.getTrainCode()).append(" - leave by ").append(toShow).append("\n");
            }
        }
        NotificationHelper.getInstance().createNotification(getContext(),
                getParams().getId(), title, message.toString().trim());
    }

    @Override
    protected void onCancel() {
        countDownLatch.countDown();
        super.onCancel();
    }

    int schedule(long startMs, long endMs) {
        return DailyJob.schedule(new JobRequest.Builder(TAG), startMs, endMs);
    }

    private boolean hasToRun(Commute commute) {
        switch (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                if (commute.isMonday()) {
                    return true;
                }
                break;
            case Calendar.TUESDAY:
                if (commute.isTuesday()) {
                    return true;
                }
                break;
            case Calendar.WEDNESDAY:
                if (commute.isWednesday()) {
                    return true;
                }
                break;
            case Calendar.THURSDAY:
                if (commute.isThursday()) {
                    return true;
                }
                break;
            case Calendar.FRIDAY:
                if (commute.isFriday()) {
                    return true;
                }
                break;
            case Calendar.SATURDAY:
                if (commute.isSaturday()) {
                    return true;
                }
                break;
            case Calendar.SUNDAY:
                if (commute.isSunday()) {
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }
}
