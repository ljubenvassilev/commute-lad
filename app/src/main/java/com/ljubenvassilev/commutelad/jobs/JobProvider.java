package com.ljubenvassilev.commutelad.jobs;

import android.content.Context;

import com.evernote.android.job.JobManager;
import com.ljubenvassilev.commutelad.util.NotificationHelper;

import java.util.concurrent.TimeUnit;

public class JobProvider {

    private static final int NOTIFICATION_TIME_RETRACTION = 1;
    private static JobProvider INSTANCE;
    private CommuteCheckJob commuteCheckJob;

    private JobProvider() {
        commuteCheckJob = new CommuteCheckJob();
    }

    public static JobProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JobProvider();
        }
        return INSTANCE;
    }

    public int addJob (int time) {
        int hour = time / 60 - NOTIFICATION_TIME_RETRACTION;
        int minute = time % 60;
        long startMs = TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(minute);
        long endMs = startMs + TimeUnit.MINUTES.toMillis(1);
        return commuteCheckJob.schedule(startMs, endMs);
    }

    public void cancelJob (Context context, int jobId) {
        NotificationHelper.getInstance().cancelNotification(context, jobId);
        JobManager.instance().cancel(jobId);
    }
}
