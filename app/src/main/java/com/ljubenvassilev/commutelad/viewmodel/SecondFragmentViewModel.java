package com.ljubenvassilev.commutelad.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.repository.CommuteRepository;
import com.ljubenvassilev.commutelad.exceptions.EmptyTitleException;
import com.ljubenvassilev.commutelad.jobs.JobProvider;
import com.ljubenvassilev.commutelad.exceptions.NoDaySelectedException;
import com.ljubenvassilev.commutelad.exceptions.SameStationsException;
import com.ljubenvassilev.commutelad.repository.StationRepository;
import com.ljubenvassilev.commutelad.model.Station;

import java.util.List;

public class SecondFragmentViewModel extends AndroidViewModel {

    private StationRepository stationRepository;
    private CommuteRepository commuteRepository;
    private LiveData<List<Station>> listLiveData;
    private int time;
    private Station start;
    private Station destination;
    private int distance;
    private int id = -1;

    public SecondFragmentViewModel(@NonNull Application application) {
        super(application);
        stationRepository = new StationRepository(application);
        listLiveData = stationRepository.getAllStations();
        commuteRepository = new CommuteRepository(application);
    }

    public LiveData<List<Station>> getListLiveData() {
        return listLiveData;
    }

    public void setStart(Station start) {
        this.start = start;
    }

    public void setDestination(Station destination) {
        this.destination = destination;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LiveData<Commute> getCommuteToEdit(int commuteId) {
        return commuteRepository.getCommuteById(commuteId);
    }

    public void createCommute(@NonNull String title, boolean monday, boolean tuesday,
                              boolean wednesday, boolean thursday, boolean friday, boolean saturday,
                              boolean sunday, int id)
            throws EmptyTitleException, NoDaySelectedException, SameStationsException {
        if (title.length() == 0) {
            throw new EmptyTitleException();
        }
        if (!(monday || tuesday || wednesday || thursday || friday || saturday || sunday)) {
            throw new NoDaySelectedException();
        }
        if (start.equals(destination)) {
            throw new SameStationsException();
        }
        Commute commuteToSave = new Commute(title, start.getStationDesc(),
                destination.getStationDesc(), distance, time, monday, tuesday, wednesday, thursday,
                friday, saturday, sunday, true, JobProvider.getInstance().addJob(time));
        if (id < 0) {
            commuteRepository.insert(commuteToSave);
        } else {
            commuteToSave.setId(id);
            commuteRepository.updateCommute(commuteToSave);
        }
    }
}
