package com.ljubenvassilev.commutelad.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.repository.CommuteRepository;
import com.ljubenvassilev.commutelad.jobs.JobProvider;

import java.util.List;

public class FirstFragmentViewModel extends AndroidViewModel {

    private CommuteRepository repo;
    private LiveData<List<Commute>> listLiveData;

    public FirstFragmentViewModel(@NonNull Application application) {
        super(application);
        repo = new CommuteRepository(application);
        listLiveData = repo.getAllCommutes();
    }

    public void saveCommute(Commute commute) {
        repo.insert(commute);
    }

    public LiveData<List<Commute>> getListLiveData() {
        return listLiveData;
    }

    public void changeCommute(Context context, Commute commute, boolean enabled) {
        if (enabled) {
            commute.setJob(JobProvider.getInstance().addJob(commute.getTime()));
        } else {
            JobProvider.getInstance().cancelJob(context, commute.getJob());
            commute.setJob(-1);
        }
        commute.setEnabled(enabled);
        repo.updateCommute(commute);
    }

    public void deleteCommute(Context context, Commute commute) {
        JobProvider.getInstance().cancelJob(context, commute.getJob());
        repo.deleteCommute(commute);
    }
}
