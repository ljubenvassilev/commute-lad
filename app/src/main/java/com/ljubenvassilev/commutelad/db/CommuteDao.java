package com.ljubenvassilev.commutelad.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.ljubenvassilev.commutelad.model.Commute;

import java.util.List;

@Dao
public interface CommuteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Commute commute);

    @Query("SELECT * from commute_table ORDER BY `id` ASC")
    LiveData<List<Commute>> getAllCommutes();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Commute commute);

    @Delete
    void delete(Commute param);

    @Query("SELECT * FROM commute_table WHERE id=:id ")
    LiveData<Commute> getCommuteById(int id);

    @Query("SELECT * FROM commute_table WHERE job=:job")
    Commute getCommuteByJob(int job);
}
