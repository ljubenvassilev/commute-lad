package com.ljubenvassilev.commutelad.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ljubenvassilev.commutelad.model.Commute;
import com.ljubenvassilev.commutelad.model.Station;

@Database(entities = {Commute.class, Station.class}, version = 1, exportSchema = false)
public abstract class CommuteRoomDatabase extends RoomDatabase {

    public abstract CommuteDao getCommuteDao();
    public abstract StationDao getStationDao();

    private static CommuteRoomDatabase INSTANCE;

    public static CommuteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CommuteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CommuteRoomDatabase.class, "commute_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
