package com.ljubenvassilev.commutelad.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ljubenvassilev.commutelad.model.Station;

import java.util.List;

@Dao
public interface StationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Station station);

    @Query("SELECT * from station_table ORDER BY `desc` ASC")
    LiveData<List<Station>> getAllStations();
}
