package com.ljubenvassilev.commutelad.api;

import com.ljubenvassilev.commutelad.model.AllStationData;
import com.ljubenvassilev.commutelad.model.AllStations;
import com.ljubenvassilev.commutelad.model.AllTrainMovements;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/realtime/realtime.asmx/getAllStationsXML")
    Call<AllStations> getAllStations();

    @GET("/realtime/realtime.asmx/getStationDataByNameXML")
    Call<AllStationData> getAllStationData(@Query("StationDesc") String stationDesc);

    @GET("/realtime/realtime.asmx/getTrainMovementsXML")
    Call<AllTrainMovements> getAllTrainMovements(@Query("TrainId") String trainId,
                                                 @Query("TrainDate") String trainDate);
}
