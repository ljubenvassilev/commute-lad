package com.ljubenvassilev.commutelad.api;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ApiProvider {

    private static ApiProvider INSTANCE;
    private ApiService apiService;

    private ApiProvider() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.irishrail.ie")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public static ApiProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiProvider();
        }
        return INSTANCE;
    }

    public ApiService getApiService() {
        return apiService;
    }
}
